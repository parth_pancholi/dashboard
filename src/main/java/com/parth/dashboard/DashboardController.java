package com.parth.dashboard;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;

/**
 * @author parth
 *
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class DashboardController {

	public static final String directory = "/home/ubuntu/data/";

//	 public static final String directory =
//	 "/home/parth/parth/bhargavi/dashboard/data/";
	@GetMapping(path = "/getMasterData", produces = "application/json")
	public static String getMasterData() {
		File directory = new File("/home/ubuntu/data/");
//		File directory = new File("/home/parth/parth/bhargavi/dashboard/data");
		// get all the files from a directory
		File[] fList = directory.listFiles();
		List test1 = new ArrayList<String>();
		for (File file : fList) {
			HashMap<String, Object> map = new HashMap<String, Object>();
			if (file.isFile()) {
				List<String> test = new ArrayList<String>();
				try {
					Database db = DatabaseBuilder.open(new File(file.getAbsolutePath()));
							db.getTableNames().parallelStream()
							.forEach(action -> {
								test.add(action);
							});
					map.put("constituency", file.getName().split("\\.")[0]);
					map.put("booth", test);
					db.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			test1.add(map);
		}
		return new Gson().toJson(test1);
	}

	/**
	 * @param fileName
	 * @param boothName
	 * @return
	 */
	@GetMapping(path = "/getBoothDetails/{fileName}/{boothName}", produces = "application/json")
	public static String getBoothDetails(@PathVariable("fileName") String fileName,
			@PathVariable("boothName") String boothName) {
		HashMap<String, String> tempMap;
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			Connection conn = DriverManager.getConnection("jdbc:ucanaccess://" + directory + fileName + ".mdb", "", "");
			Statement s = conn.createStatement();
			// Fetch table
			String selTable = "select part_no from " + boothName
					+ " limit 1";
			s.execute(selTable);
			ResultSet rs = s.getResultSet();
			tempMap = new HashMap<String, String>();
			while ((rs != null) && (rs.next())) {
				tempMap.put("reportPrefix","बूथ क्र. ("+rs.getInt("part_no") + ") ");
			}
			// close and cleanup
			s.close();
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Gson().toJson("{'message': '" + ex.getLocalizedMessage() + "'}");
		}
		return new Gson().toJson(tempMap);
	}

	/**
	 * @param fileName
	 * @param boothName
	 * @return
	 */
	@GetMapping(path = "/report1/{fileName}/{boothName}", produces = "application/json")
	public String getReport1(@PathVariable("fileName") String fileName, @PathVariable("boothName") String boothName) {
		ArrayList<Object> resultList = new ArrayList<Object>(1);
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			Connection conn = DriverManager.getConnection("jdbc:ucanaccess://" + directory + fileName + ".mdb", "", "");
			Statement s = conn.createStatement();
			// Fetch table
			String selTable = "select sex, COUNT(*) as num from " + boothName + " group by sex";
			s.execute(selTable);
			ResultSet rs = s.getResultSet();
			HashMap<String, String> tempMap;
			while ((rs != null) && (rs.next())) {
				if(rs.getString("sex") != null) {
					tempMap = new HashMap<String, String>();
					tempMap.put("sex", rs.getString("sex"));
					tempMap.put("num", rs.getString("num"));
					resultList.add(tempMap);
				}
			}
			// close and cleanup
			s.close();
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Gson().toJson("{'message': '" + ex.getLocalizedMessage() + "'}");
		}
		return new Gson().toJson(resultList);
	}

	/**
	 * @param fileName
	 * @param boothName
	 * @return
	 */
	@GetMapping(path = "/report2/{fileName}/{boothName}")
	public String getReport2(@PathVariable("fileName") String fileName, @PathVariable("boothName") String boothName) {
		ArrayList<Object> resultList = new ArrayList<Object>(1);
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			Connection conn = DriverManager.getConnection("jdbc:ucanaccess://" + directory + fileName + ".mdb", "", "");
			Statement s = conn.createStatement();
			// Fetch table
			String selTable = "select LASTNAME_V1, COUNT(*) as num from " + boothName
					+ " group by LASTNAME_V1";
			s.execute(selTable);
			ResultSet rs = s.getResultSet();
			HashMap<String, String> tempMap;
			while ((rs != null) && (rs.next())) {
				if (rs.getString("LASTNAME_V1")!=null && !"".equals(rs.getString("LASTNAME_V1")) && !".".equals(rs.getString("LASTNAME_V1")) && Integer.valueOf(rs.getString("num")) >= 10) {
					tempMap = new HashMap<String, String>();
					tempMap.put("surname", rs.getString("LASTNAME_V1"));
					tempMap.put("num", rs.getString("num"));
					resultList.add(tempMap);
				}
			}
			// close and cleanup
			s.close();
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Gson().toJson("{'message': '" + ex.getLocalizedMessage() + "'}");
		}
		return new Gson().toJson(resultList);
	}

	/**
	 * @param fileName
	 * @param boothName
	 * @return
	 */
	@GetMapping(path = "/report3/{fileName}/{boothName}")
	public String getReport3(@PathVariable("fileName") String fileName, @PathVariable("boothName") String boothName) {
		ArrayList<HashMap<String, String>> resultList = new ArrayList<HashMap<String, String>>(1);
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			Connection conn = DriverManager.getConnection("jdbc:ucanaccess://" + directory + fileName + ".mdb", "", "");
			Statement s = conn.createStatement();
			// Fetch table
			String selTable = "select section_nm,section_no, sex, COUNT(*) as num from " + boothName
					+ " group by section_no,sex,section_nm";
			s.execute(selTable);
			ResultSet rs = s.getResultSet();
			HashMap<String, String> tempMap;
			while ((rs != null) && (rs.next())) {
				if(rs.getString("sex") != null && rs.getString("section_nm")!=null) {
					tempMap = new HashMap<String, String>();
					tempMap.put("SECTIONNO", rs.getString("section_no"));
					tempMap.put("sectionnamehindi", rs.getString("section_nm"));
					tempMap.put("sex", rs.getString("sex"));
					tempMap.put("num", rs.getString("num"));
					resultList.add(tempMap);
				}
			}
			// close and cleanup
			s.close();
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Gson().toJson("{'message': '" + ex.getLocalizedMessage() + "'}");
		}
		return new Gson().toJson(resultList);
	}

	/**
	 * @param fileName
	 * @param boothName
	 * @return
	 */
	@GetMapping(path = "/report4/{fileName}/{boothName}")
	public String getReport4(@PathVariable("fileName") String fileName, @PathVariable("boothName") String boothName) {
		ArrayList<Object> resultList = new ArrayList<Object>(1);
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			Connection conn = DriverManager.getConnection("jdbc:ucanaccess://" + directory + fileName + ".mdb", "", "");
			Statement s = conn.createStatement();
			// Fetch table
			String selTable = "select Age, COUNT(*) as num from " + boothName + " group by Age";
			s.execute(selTable);
			ResultSet rs = s.getResultSet();
			HashMap<String, Object> tempMap;
			while ((rs != null) && (rs.next())) {
				if(rs.getInt("Age") > 0) {
					tempMap = new HashMap<String, Object>();
					tempMap.put("age", Integer.valueOf(rs.getInt("Age")));
					tempMap.put("num", rs.getString("num"));
					resultList.add(tempMap);
				}
			}
			// close and cleanup
			s.close();
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Gson().toJson("{'message': '" + ex.getLocalizedMessage() + "'}");
		}
		return new Gson().toJson(resultList);
	}

	/**
	 * @param fileName
	 * @param boothName
	 * @return
	 */
	@GetMapping(path = "/report6/{fileName}/{boothName}")
	public String getReport6(@PathVariable("fileName") String fileName, @PathVariable("boothName") String boothName) {
		TreeMap<String, Integer> tempMap;
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			Connection conn = DriverManager.getConnection(
					"jdbc:ucanaccess://"+directory + fileName + ".mdb", "", "");
			Statement s = conn.createStatement();
			// Fetch table
			String selTable = "select Age, COUNT(*) as num from " + boothName + " group by Age";
			s.execute(selTable);
			ResultSet rs = s.getResultSet();
			tempMap = new TreeMap<String, Integer>();
			
			while ((rs != null) && (rs.next())) {
				if(rs.getInt("Age") > 0) {
					if(Integer.valueOf(rs.getInt("Age")) >= 18 && Integer.valueOf(rs.getInt("Age")) <= 30) {
						tempMap.put("18-30", tempMap.containsKey("18-30") ? tempMap.get("18-30") + Integer.valueOf(rs.getInt("num")) : Integer.valueOf(rs.getInt("num")));
					}else if(Integer.valueOf(rs.getInt("Age")) >= 31 && Integer.valueOf(rs.getInt("Age")) <= 40) {
						tempMap.put("31-40", tempMap.containsKey("31-40") ? tempMap.get("31-40") + Integer.valueOf(rs.getInt("num")) : Integer.valueOf(rs.getInt("num")));
					}else if(Integer.valueOf(rs.getInt("Age")) >= 41 && Integer.valueOf(rs.getInt("Age")) <= 60) {
						tempMap.put("41-60", tempMap.containsKey("41-60") ? tempMap.get("41-60") + Integer.valueOf(rs.getInt("num")) : Integer.valueOf(rs.getInt("num")));
					}else if(Integer.valueOf(rs.getInt("Age")) >= 61 && Integer.valueOf(rs.getInt("Age")) <= 80){
						tempMap.put("61-80", tempMap.containsKey("61-80") ? tempMap.get("61-80") + Integer.valueOf(rs.getInt("num")) : Integer.valueOf(rs.getInt("num")));
					}else {
						tempMap.put("80+", tempMap.containsKey("80+") ? tempMap.get("80+") + Integer.valueOf(rs.getInt("num")) : Integer.valueOf(rs.getInt("num")));
					}
				}
			}
			// close and cleanup
			s.close();
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Gson().toJson("{'message': '" + ex.getLocalizedMessage() + "'}");
		}
		return new Gson().toJson(tempMap);
	}

	/**
	 * @param fileName
	 * @param boothName
	 * @return
	 */
	@GetMapping(path = "/report5/{fileName}/{boothName}")
	public String getReport5(@PathVariable("fileName") String fileName, @PathVariable("boothName") String boothName) {
		// HashMap<String,String> result = new HashMap<String,String>();
		ArrayList<Object> resultList = new ArrayList<Object>(1);
		try {
			Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			Connection conn = DriverManager.getConnection("jdbc:ucanaccess://" + directory + fileName + ".mdb", "", "");
			Statement s = conn.createStatement();
			// Fetch table
			String selTable = "select section_nm,house_no, COUNT(*) as num, section_no , LASTNAME_V1 from "
					+ boothName + " group by house_no, section_no , LASTNAME_V1,section_nm";
			s.execute(selTable);
			ResultSet rs = s.getResultSet();
			HashMap<String, String> tempMap;
			while ((rs != null) && (rs.next())) {
				if (rs.getString("house_no")!= null && rs.getString("LASTNAME_V1")!=null && rs.getString("section_nm")!=null && !"".equals(rs.getString("LASTNAME_V1")) && !".".equals(rs.getString("LASTNAME_V1")) && Integer.valueOf(rs.getString("num")) >= 5) {
					tempMap = new HashMap<String, String>();
					tempMap.put("HouseNumber", rs.getString("house_no"));
					tempMap.put("num", rs.getString("num"));
					tempMap.put("SECTIONNO", rs.getString("section_no"));
					tempMap.put("Surnameinenglish", rs.getString("LASTNAME_V1"));
					tempMap.put("sectionnamehindi", rs.getString("section_nm"));
					resultList.add(tempMap);
				}
			}
			// close and cleanup
			s.close();
			conn.close();
		} catch (Exception ex) {
			ex.printStackTrace();
			return new Gson().toJson("{'message': '" + ex.getLocalizedMessage() + "'}");
		}
		return new Gson().toJson(resultList);
	}
}
